# Technical Test for Software Testing Role

## Objective

Evaluate the candidate's proficiency in testing, using tools like Postman, Cypress or Pytest, as well as knowledge of Git, GitLab, Jira, and a test management tool.

### Task 1: Test Case Creation (1 hour)

Given a sample application that allows users to register, login, and update their profile:

Write 5 test cases for the login functionality using a test management tool of your choice. Make sure to include both positive and negative test cases.

### Task 2: Automation Using Pytest/Cypress (1.5 hours)

- Set up a basic testing framework using either Pytest (if using Python) or Cypress (if using JavaScript).
- Write automated tests for the following scenarios:
  Successful user registration.
  Login with valid credentials.
  Login with invalid credentials.

### Task 3: API Testing with Postman (1 hour)

Given a set of API endpoints:

- Import the API collection into Postman.
- Write tests in Postman to:
  Check if a GET request to fetch a user's details is successful.
  Check if a PUT request to update a user's details works correctly.

### Task 5: Issue Tracking with Jira (30 minutes)

- Imagine you found a bug in the sample application while testing. Create a bug ticket in Jira (you can use a free Jira account for this).
- Make sure to include all necessary details in the bug ticket: description, steps to reproduce, expected result, actual result, and any attachments (screenshots or logs).
- Share the link to the bug ticket with us.

## Evaluation Criteria:

- _Completeness:_ All tasks should be completed within the given time frame.
- _Quality:_ Test cases should be clear, concise, and cover a range of scenarios. Automated tests should run without errors and cover the scenarios described.
- _Documentation:_ Proper comments and documentation in code/tests.
- _Best Practices:_ Usage of Git best practices, meaningful commit messages, and proper organization of Jira tickets.

## Getting started

Welcome to the frontend test for prospective candidates.

**Instructions for Collaborating on Our GitLab Project**:

1. **Create a GitLab Account**:

   - If you don't already have an account, visit [GitLab](https://gitlab.com/) and sign up for free.

2. **Provide Your Email Address**:

   - Share your email address with us, so we can send you an invitation to access the project.

3. **Begin Testing**:
   - Once you've gained access, you can start executing the test scenarios provided.

Make sure to notify us of any findings or if you require further assistance!

## Test Scenario:

### Test 1 perfomed user login (30 minutes)

1. Access the URL: https://phone.aircall-staging.com/login
2. Enter email **hire_cypress_test@23ca45c.mailosaur.net**
3. Enter password **K23W$0txDx1p8)A+4XqU**
4. Click on the submit Button to performed a Login.
5. Verify or check if the user is logged in.

### Test 2 create contact from people view (1 hour)

- **Steps:**

1. Login to phone app URL: https://phone.aircall-staging.com/login
2. Goto people view
3. Click on Add
4. Create a contact
   - FirstName: Hire
   - LastName: Contact test
   - Number: +61 1900 654 321

### Requirements:

1. Use JavaScript and optionally Cypress.
2. [Installing Cypress Guide](https://docs.cypress.io/guides/getting-started/installing-cypress)
3. Knowledge of Postman.
4. Proficient with Git and GitLab.
5. Familiarity with JIRA and a test management tool.

### Submission:

1. Clone this repository.
2. Complete the test.
3. Push your changes and create a merge request.
